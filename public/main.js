const nextButtonComponent = () => ({
    init() {
      const playPlane = document.querySelector("#playbtn");
      playPlane.addEventListener("click", (event) => {
        document.querySelector("#vid").play();
        playPlane.remove();
      });
      const mailBtn = document.querySelector("#mailbtn");
      mailBtn.addEventListener("click", (event) => {
        window.open("mailto:test@gmail.com", "_blank");
      });
  
      const phoneBtn = document.querySelector("#phonebtn");
      phoneBtn.addEventListener("click", (event) => {
        window.open("tel:+94761911551", "_blank");
      });
  
      const instabtn = document.querySelector("#instabtn");
      instabtn.addEventListener("click", (event) => {
        window.open("https://www.instagram.com/test/", "_blank");
      });
  
      const linebtn = document.querySelector("#linebtn");
      linebtn.addEventListener("click", (event) => {
        window.open("https://line.me", "_blank");
      });
  
      const web1Btn = document.querySelector("#web1btn");
      web1Btn.addEventListener("click", (event) => {
        window.open("https://google.com", "_blank");
      });
  
      const fbbtn = document.querySelector("#fbbtn");
      fbbtn.addEventListener("click", (event) => {
        window.open("https://www.facebook.com/", "_blank");
      });
  
      const youtubeBtn = document.querySelector("#youtubebtn");
      youtubeBtn.addEventListener("click", (event) => {
        window.open("https://www.youtube.com/", "_blank");
      });
  
      const linktreebtn = document.querySelector("#linktreebtn");
      linktreebtn.addEventListener("click", (event) => {
        window.open("https://linktr.ee/", "_blank");
      });
    },
  });
  export { nextButtonComponent };
  